from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules = [
    Extension("echidna",
              sources=["echidna.pyx"],
              libraries=["SDL2", "SDL2_image"]
    )
]

setup(
    name="echidna",
    ext_modules=cythonize(ext_modules)
)
