#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL.h>
#include <SDL_image.h>

#include "globals.h"
#include "input.h"
#include "graphics.h"
#include "time.h"

/*
 * TODO:
 * - Funciones de mover usando callbacks
 * - queue de gameobjects 
 */

void init(void) {
    initGraphics();
    mainGame.time = 0;
}

void cleanup(void) {
    IMG_Quit();
	SDL_Quit();
}

typedef struct {
    SDL_Texture* texture;
    size_t w;
    size_t h;
    size_t x;
    size_t y;
    size_t n;
} Sprite;

typedef struct {
    char name[32];
    struct Position {
        double x; 
        double y;
    } position;
    Sprite sprite;
} GameObject;


GameObject* newGameObject(char* name, double x, double y) {
    GameObject* ret = malloc(sizeof(GameObject));
    strcpy(ret->name,  name);
    ret->position.x = x;
    ret->position.y = y;
    ret->sprite.texture = NULL;
    ret->sprite.w = 32;
    ret->sprite.h = 32;
    ret->sprite.n = 27;
    ret->sprite.x = 0;
    ret->sprite.y = 0;
    
    return ret;
}

inline void assignTexture(GameObject* gObject, char path[]) {
    SDL_Texture* texture = loadTexture(path);
    gObject->sprite.texture = texture;
}

void move(GameObject* gameObject, double xDelta, double yDelta) {
    gameObject->position.x += xDelta;
    gameObject->position.y += yDelta;
}

void frameStart(void) {
    /*SDL_FillRect(game->screenSurface,
                 NULL,
                 SDL_MapRGB(game->screenSurface->format,
                            0x00,
                            0xFF,
                            0x00));
                            */
    mainGame.time = SDL_GetTicks();
    SDL_RenderClear(graphics.renderer);
    updateTime();
}

void frameEnd(void) {
    //XXX: al parecer esto es chungo y no sirve
    if((SDL_GetTicks() - mainGame.time) < TICKS_PER_FRAME) {
        SDL_Delay(TICKS_PER_FRAME - SDL_GetTicks() + mainGame.time);
    }
    SDL_RenderPresent(graphics.renderer);
    updateTime();
    fprintf(stdout, "dt: %d\n", gTime.deltaTime);
    fflush(stdout);
}

struct {
   bool keys[N_SDL_KEYS]; 
   void (*actions[N_SDL_KEYS])(GameObject*);
} gInput;

extern inline void doNothing(GameObject* g) {}

extern inline void moveRight(GameObject* g) {
    g->position.x += 0.1;
}

extern inline void moveLeft(GameObject* g) {
    g->position.x -= 0.1;
}

extern inline void moveUp(GameObject* g) {
    g->position.y -= 0.1;
}

extern inline void moveDown(GameObject* g) {
    g->position.y += 0.1;
}

void Input_init(void) {
    for(int i=0; i<N_SDL_KEYS; i++) {
        gInput.keys[i] = false;
        gInput.actions[i] = &doNothing;
    }
    //:XXX:IDEA: Implementar, mediante un callback, una función de
    // setup para los controles.
    gInput.actions[SDL_SCANCODE_W] = &moveUp;
    gInput.actions[SDL_SCANCODE_A] = &moveLeft;
    gInput.actions[SDL_SCANCODE_S] = &moveDown;
    gInput.actions[SDL_SCANCODE_D] = &moveRight;
}

inline void inputKeyDown(SDL_Event e) {
    gInput.keys[e.key.keysym.scancode] = true;
}

inline void inputKeyUp(SDL_Event e) {
    gInput.keys[e.key.keysym.scancode] = false;
}

void updateSprite(Sprite* sprite) {
    
    static int i = 0;

    if (i++ % 7 != 0) {
        return;
    }

    sprite->x += sprite->w;
    if (sprite->x >= sprite->w * 5) {
        sprite->x = 0;
        sprite->y += sprite->h;
        if (sprite->y >= sprite->h * 5) {
            sprite->y = 0;
        }
    }
    if (sprite->x >= sprite->w && sprite->y >= sprite->h * 4) {
        sprite->x = 0;
        sprite->y = 0;
    }
}

inline void drawToScreen(GameObject* g, size_t w, size_t h) {
    SDL_Rect render_rect = {.x=g->position.x, .y=g->position.y, .w=w, .h=h};
    Sprite sprite = g->sprite;
    SDL_Rect sprite_rect = {sprite.x, sprite.y, sprite.w, sprite.h};
    updateSprite(&g->sprite);
    SDL_RenderCopy(graphics.renderer, g->sprite.texture, &sprite_rect, &render_rect);
    // Victor es súper guay
    // TODO: reemplazar por
    //SDL_RenderCopyEx(mainGame.renderer, g->sprite.texture, &sprite_rect, &render_rect, 0.0f, NULL, SDL_FLIP_NONE); 
}

int handleEvents(void) {
    static SDL_Event event;

    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_QUIT:
            return 0;
        case SDL_KEYDOWN:
            inputKeyDown(event);
            break;
        case SDL_KEYUP:
            inputKeyUp(event);
            break;
        }
    }
    return 1;
}

// XXX: cambiar de nombre a esta funcion
void update(GameObject* g){ 
    for(int i=0; i<N_SDL_KEYS; i++) {
        if(gInput.keys[i]) {
            gInput.actions[i](g);
        }
    }
}

