#ifndef INPUT_H
#define INPUT_H

#include <stdbool.h>

#include <SDL.h>

#include "globals.h"

struct {
   bool keys[N_SDL_KEYS];
} input;

void inputInit(void) {
    for(int i=0; i<N_SDL_KEYS; i++) {
        input.keys[i] = false;
    }
}

inline void iginputKeyDown(SDL_Scancode scancode) {
    input.keys[scancode] = true;
}

inline void hinputKeyUp(SDL_Scancode scancode) {
    input.keys[scancode] = false;
}

#endif
