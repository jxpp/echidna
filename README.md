Este es un trabajo en etapas tempranas. ¡Lo que hay ahora mismo es un poco
desastroso! Pero si te sirve para algo, ¡adelante!

# Echidna
## Motor de videojuegos en C y Python con Cython 

El propósito de este proyecto es la creación de un motor muy simple para
videojuegos 2D, utilizando Cython para integrar el código rápido de C
con la elegancia del código de Python. El resultado final será (¡Eso
espero!) un motor rápido, pequeño y fácil de usar, muy apropiado para el
prototipado rápido.

## ¿Por qué?

Este proyecto es una experiencia de aprendizaje que surge de mi amor
por Python, por C y por el desarrollo de videojuegos. Espero mi propio
viaje y lucha con el código le pueda ayudar a cualquiera que busque
aprender.

## ¿Cómo?

Primero, necesitarás haber instalado los siguientes paquetes en tu
instalación de GNU/Linux:

* python3.6
* gcc
* libsdl2-dev

Después debes contar con `pip` e instalar los paquetes de python en
[`requirements.txt`](requirements.txt) con:

    pip install -r requirements.txt

Ahora, para correr el código es muy sencillo:

1. Ejecuta la makefile con el comando `make all` y se realizará toda la
   configuración necesaria.
2. Ejecuta el código de Python del fichero [`test.py`](test.py)

¡Más sencillo no podría ser!
