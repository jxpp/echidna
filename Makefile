CFLAGS:="$(shell sdl2-config --cflags) -Wall -Werror -pedantic -std=c11 -O3 -g"
LDFLAGS:="$(shell sdl2-config --libs)"
PSETUP:=python setup.py build_ext
PSETUP_FLAGS:=--inplace


all: venv
	CFLAGS=$(CFLAGS) LDFLAGS=$(LDFLAGS) $(PSETUP) $(PSETUP_FLAGS) 

run: all
	python test.py

force: venv
	CFLAGS=$(CFLAGS) LDFLAGS=$(LDFLAGS) $(PSETUP) $(PSETUP_FLAGS) -f

.PHONY: clean
clean: venv
	python setup.py clean

#.PHONY: venv
venv:
	. bin/activate
