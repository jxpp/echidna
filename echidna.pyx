import sys


cdef extern from "globals.h":
    struct mainGame


cdef extern from "engine.c":
    ctypedef struct Game:
        pass

    ctypedef struct Sprite:
        pass

    ctypedef struct Position:
        double x
        double y

    ctypedef struct GameObject:
        char* name
        Position position
        Sprite sprite

    ctypedef struct Input:
        pass

    void init()

    void frameStart()
    void frameEnd()

    GameObject* newGameObject(char*, double, double)
    void move(GameObject*, double, double)

    int handleEvents()
    void Input_init()
    void doNothing(GameObject*)
    
    void update(GameObject*)

    void assignTexture(GameObject*, char[])
    void drawToScreen(GameObject*, size_t, size_t)


cdef class PyGameObject:
    cdef GameObject* g

    def __init__(self, name, double x, double y):
        self.g = newGameObject(name, x, y)

    cpdef move(self, double xDelta, double yDelta):
        move(self.g, xDelta, yDelta)

    cpdef get_name(self):
        return self.g.name

    cpdef get_x(self):
        return self.g.position.x

    cpdef get_y(self):
        return self.g.position.y

    cpdef update(self):
        update(self.g)

    cpdef draw(self):
        drawToScreen(self.g, 300, 300)

    def __str__(self):
        return f"({self.g.name}){{x:{self.g.position.x}, y:{self.g.position.y}}}"

class renderingSystem:

    def __init__(self):
        self.components = set()

    def register_component(self, component):
        self.components.add(component)

    def deregister_component(self, component):
        self.components.remove(component)
    
    def update(self):
        for component in self.components:
            component.draw()

class inputSystem:

    def __init__(self):
        self.components = set()

    def register_component(self, component):
        self.components.add(component)

    def deregister_component(self, component):
        self.components.remove(component)

    def update(self):
        for component in self.components:
            component.update()


cpdef void main_loop():
    init()


    pedro = PyGameObject(b"pedro", -300, 0)
    assignTexture(pedro.g, "ball.png")
    paco = PyGameObject(b"paco", 20, 30)
    assignTexture(paco.g, "ball.png")
    luis = PyGameObject(b"luis", 40, 50)
    assignTexture(luis.g, "ball.png")
    jose = PyGameObject(b"jose", 100, -10)
    assignTexture(jose.g, "ball.png")

    game_objects = [pedro, paco, luis, jose]

    render = renderingSystem()
    inputS = inputSystem()
    for game_object in game_objects:
        render.register_component(game_object)
        inputS.register_component(game_object)

    systems = [inputS, render]

    Input_init()

    running = True
    while running:
        frameStart()
        running = handleEvents()
        for system in systems:
            system.update()
        frameEnd()

