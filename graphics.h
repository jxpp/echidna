#ifndef GRAPHICS_H 
#define GRAPHICS_H

#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>

#include "globals.h"

struct {
    SDL_Window* window;
    SDL_Surface* screenSurface;
    SDL_Renderer* renderer;
} graphics = {NULL, NULL, NULL};

void initGraphics(void) {
    SDL_Window* window = NULL;
    SDL_Surface* screenSurface = NULL;

    SDL_Renderer* renderer = NULL;
	//Initialize SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        exit(1);
	}
    window = SDL_CreateWindow(TITLE,
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              SCREEN_WIDTH,
                              SCREEN_HEIGHT,
                              SDL_WINDOW_SHOWN);
    if(window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    int imgFlags = IMG_INIT_PNG;
    if(!(IMG_Init( imgFlags ) & imgFlags)) {
        printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
        exit(1);
    }

    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

    //Get window surface
    screenSurface = SDL_GetWindowSurface(window);

    graphics.window = window;
    graphics.screenSurface = screenSurface;
    graphics.renderer = renderer;
}

SDL_Texture* loadTexture(char path[]) {
	//The final texture
	SDL_Texture* newTexture = NULL;
	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path);
	if(loadedSurface == NULL) {
		printf("Unable to load image %s! SDL_image Error: %s\n", path, IMG_GetError());
		return NULL;
	}
		//Create texture from surface pixels
	newTexture = SDL_CreateTextureFromSurface(graphics.renderer, loadedSurface);
	if(newTexture == NULL) {
		printf("Unable to create texture from %s! SDL Error: %s\n", path, SDL_GetError());
		return NULL;
	}
	//Get rid of old loaded surface
	SDL_FreeSurface(loadedSurface);
	return newTexture;
}

typedef struct {
    size_t nFrames;
    SDL_Texture** frames;
} AnimatedSprite;

typedef struct {
    SDL_Texture* texture;
} StaticGraphicsComponent;

AnimatedSprite newAnimatedSprite(char filename[], size_t w, size_t h, size_t n) {
    //SDL_Texture* spritesheet = loadTexture(filename);
    AnimatedSprite sprite;
    sprite.nFrames = n;
    sprite.frames = malloc(sizeof(SDL_Texture*) * sprite.nFrames);
    for(int i=0; i<n; i++) {
        
    }
    return sprite;
}

#endif
